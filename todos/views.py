from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoForm, TodoItemForm


def list_view(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def detail_view(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_list_detail": todo_detail}
    return render(request, "todos/detail.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(
            request.POST, instance=todo_list, initial={"name": todo_list.name}
        )
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo_list)
    context = {"todo_list": todo_list, "form": form}
    return render(request, "todos/edit.html", context)


def delete_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        item_form = TodoItemForm(request.POST)

        if item_form.is_valid():
            item = item_form.save()

            return redirect("todo_list_detail", id=item.list.id)
    else:
        item_form = TodoItemForm()
    context = {
        "form": item_form,
    }
    return render(request, "todos/items/create.html", context)


def update_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    options = todo_item.list
    if request.method == "POST":
        item_form = TodoItemForm(
            request.POST,
            instance=todo_item,
        )
        if item_form.is_valid():
            item = item_form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        item_form = TodoItemForm(instance=todo_item)
    context = {
        "todo_item": todo_item,
        "form": item_form,
        "options": options,
    }
    return render(request, "todos/items/edit.html", context)
