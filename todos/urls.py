from django.urls import path
from todos.views import (
    list_view,
    detail_view,
    create_view,
    update_view,
    delete_view,
    create_item,
    update_item,
)

urlpatterns = [
    path("todos/", list_view, name="list_view"),
    path("todos/<int:id>/", detail_view, name="todo_list_detail"),
    path("todos/create/", create_view, name="todo_list_create"),
    path("todos/<int:id>/edit/", update_view, name="todo_list_update"),
    path("todos/<int:id>/delete/", delete_view, name="todo_list_delete"),
    path("todos/items/create/", create_item, name="todo_item_create"),
    path("todos/items/<int:id>/edit/", update_item, name="todo_item_update"),
    path("", list_view, name="todo_list_list"),
]
